import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Picker,
  Button,
  Alert,
} from 'react-native';
import {
  StackNavigator,
} from 'react-navigation';

var REQUEST_URL = 'http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b1b15e88fa797225412429c1c50c122a1';

//Second screen
class DisplayScreen extends Component {
  // set screen title
  static navigationOptions = {
    title: 'Display',
  }

  // Initialize state
  constructor() {
    super();
    this.state = {
      resultJson: null,
    };
  }

  //After mount, fetch data
  componentDidMount() {
    this.fetchData();
  }

  // fetch data, then set result to state
  fetchData() {
    const {params} = this.props.navigation.state;
    return fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          resultJson: responseJson,
        })
      })
      .catch((error) => {
        console.error(error);
      });
  }

  // Show result from network
  render() {
    if(this.state.resultJson == null) {
      resultJSON = 'Loading....'
    } else {
      resultJSON = JSON.stringify(this.state.resultJson);
    }
    const {params} = this.props.navigation.state;
    return(
      <View style={styles.container}>
        <Text>Display {params.province} </Text>
        <Text> {resultJSON} </Text>
      </View>
    );
  }
}

//Home screen
class HomeScreen extends Component {
  //Set screen title
  static navigationOptions = {
    title: 'Home',
  }
  //Initialize state
  constructor() {
    super();
    this.state = {
      province: "lpn",
    };
  }
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
        <Picker
          style={styles.picker}
          selectedValue={this.state.province}
          onValueChange={(name) => this.setState({province: name})}
          mode="dropdown"
          >
          <Picker.Item label="Lamphun" value="Lamphun" />
          <Picker.Item label="Lampang" value="Lampang" />
          <Picker.Item label="Chiang Mai" value="Chiang Mai" />
        </Picker>
        <Button
          title="Submit"
          onPress={() => 
            navigate('Display', {province: this.state.province}) //navigate to second screen
          }
          />
      </View>
    );
  }
}

//Style sheet
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  picker: {
    width: 200,
  }
});

//Screen set
const WeatherMonitorApp = StackNavigator({
  Home: {screen: HomeScreen},
  Display: {screen: DisplayScreen},
});

AppRegistry.registerComponent('WeatherMonitor', () => WeatherMonitorApp);
